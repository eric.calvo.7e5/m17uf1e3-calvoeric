using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public string name;
    public string typeClass;
    public float height;
    public float weight;
    public float velocity;
    public float distanceToTravel;
    public int Apple;

    public void SetApple(int apple)
    {
        Apple += apple;
    }

    public int GetApple()
    {
        return Apple;
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}