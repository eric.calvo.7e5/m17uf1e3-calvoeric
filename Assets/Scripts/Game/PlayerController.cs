using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool canJump;
    public Sprite[] spritesPlayer;
    private int currentFrame;
    private float timer;
    private float framerate = 1 / 12f;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey("right") || Input.GetKey("d") || Input.GetKey("left") || Input.GetKey("a") || Input.GetKey("up") || Input.GetKey("w"))
        {
            Movement();
        }
        else { Idle(); }
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.collider.tag == "ground")
        {
            canJump = true;
            Debug.Log(canJump);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        
        if (collision.collider.tag == "ground")
        {
            canJump = true;
            Debug.Log(canJump);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {

        if (collision.collider.tag == "ground")
        {
            canJump = false;
            Debug.Log(canJump);
        }
    }
    private void Movement()
    {
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000F * Time.deltaTime, 0));
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            Animate();
        }

        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000F * Time.deltaTime, 0));
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            Animate();
        }

        if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && canJump)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 800F));
            Animate();
        }
    }
    public void Idle()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = spritesPlayer[0];
    }
    public void Animate()
    {
        timer += Time.deltaTime;

        if (timer >= framerate)
        {
            timer -= framerate;
            currentFrame = (currentFrame + 1) % spritesPlayer.Length;
            spriteRenderer.sprite = spritesPlayer[currentFrame];
        }
    }
}