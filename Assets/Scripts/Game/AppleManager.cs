using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AppleManager : MonoBehaviour
{
    private void Start()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.transform.tag == "apple")
        {
            gameObject.SetActive(false);
            Debug.Log("Rica manzana papi");
            collision.gameObject.GetComponent<PlayerData>().SetApple(1);
        }
    }

    private void Update()
    {
        GameObject.Find("AppleCounter").GetComponent<TMP_Text>().text = "Apples: " + GameObject.Find("Player").GetComponent<PlayerData>().Apple;
    }
}