using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{

    public PlayerController animation;
    private bool _lookingRight;
    private Vector2 _vectorDireccion = new Vector2 (-1f,-1f);
    public GameObject tilemap;
    private bool _canMove = true;
    void Start()
    {
        
    }
    

    void Update()
    {
        if (Input.GetKey("k"))
        {
            
            if (_canMove){
                if (!_lookingRight)
                {
                    MovementLeft();
                }
                else
                {
                    MovementRight();
                }
            }
            if (_lookingRight)
            {
                _vectorDireccion = Vector2.right + Vector2.down;
            }
            else
            {
                _vectorDireccion = Vector2.left + Vector2.down;
            }
            RaycastHit2D hitFront = Physics2D.Raycast(transform.position, _vectorDireccion, 1.5f, LayerMask.GetMask("Platforms"));
            if(hitFront.collider == null)
            {
                _lookingRight = !_lookingRight;
                _canMove = false;
                Invoke("MovementPermission", 1f);
            }
            
            
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, _vectorDireccion);
    }

   private void MovementPermission()
    {
        _canMove = true;
    }
    private void MovementRight()
    {
        _lookingRight = true;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000F * Time.deltaTime, 0));
        gameObject.GetComponent<SpriteRenderer>().flipX = true;
        animation.Animate();
    }

    private void MovementLeft()
    {
        _lookingRight = false;
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000F * Time.deltaTime, 0));
        gameObject.GetComponent<SpriteRenderer>().flipX = false;
        animation.Animate();
    }
}
