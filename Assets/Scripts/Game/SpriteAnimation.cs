using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    public Sprite[] spritesPlayer;
    private int currentFrame;
    private float timer;
    private float framerate = 1 / 12f;
    public SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer.GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= framerate)
        {
            timer -= framerate;
            currentFrame = (currentFrame + 1) % spritesPlayer.Length;
            spriteRenderer.sprite = spritesPlayer[currentFrame];
        }
    }
}